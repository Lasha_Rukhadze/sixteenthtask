package com.example.sixteenthtask

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Country(
    @PrimaryKey(autoGenerate = true) var uid : Int = 0,
    @ColumnInfo(name = "country_name") var countryName : String?,
    @ColumnInfo(name = "country_capital") var countryCapital : String?,
    @ColumnInfo(name = "country_currency") var countryCurrency : String?,
    @ColumnInfo(name = "country_population") var countryPopulation : Int?
){
    constructor(countryName: String?, countryCapital: String?, countryCurrency: String?, countryPopulation: Int?) : this(0, countryName,
    countryCapital, countryCurrency, countryPopulation)
}
