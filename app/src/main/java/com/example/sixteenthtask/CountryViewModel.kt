package com.example.sixteenthtask

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CountryViewModel : ViewModel() {

    private val countryLiveData = MutableLiveData<List<Country>>()

    fun getCountryLiveData() : LiveData<List<Country>>{
        return countryLiveData
    }

    fun read(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                countryLiveData.postValue(DataBase.db.countryDao().getAll())
            }
        }
    }

    fun write(countryName: String, countryCapital: String, countryCurrency: String, countryPopulation: Int){
        val country = Country(countryName, countryCapital,countryCurrency,countryPopulation)
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                DataBase.db.countryDao().insertCountry(country)
            }
        }
    }

}