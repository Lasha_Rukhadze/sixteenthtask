package com.example.sixteenthtask

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.sixteenthtask.databinding.FragmentMainBinding
import java.lang.Integer.parseInt

class MainFragment : Fragment() {

    lateinit var binding : FragmentMainBinding
    private val viewModel : CountryViewModel by viewModels()
    private var list = mutableListOf<Country>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainBinding.inflate(inflater, container, false)

        initializer()

        return binding.root
    }

    private fun initializer(){
        observes()

        binding.write.setOnClickListener {
            viewModel.write(binding.name.text.toString(), binding.capital.text.toString(),
                binding.currency.text.toString(), parseInt(binding.population.text.toString()))
        }
        binding.read.setOnClickListener {
            viewModel.read()
        }
    }

    private fun observes(){
        viewModel.getCountryLiveData().observe(viewLifecycleOwner, {
            list.addAll(it)
        })
    }
}