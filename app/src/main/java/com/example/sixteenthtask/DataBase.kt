package com.example.sixteenthtask

import androidx.room.Room

object DataBase {

    val db : AppDataBase = Room.databaseBuilder(
        App.context!!,
        AppDataBase::class.java, "country_database"
    ).build()
}