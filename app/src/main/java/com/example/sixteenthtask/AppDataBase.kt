package com.example.sixteenthtask

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Country::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun countryDao() : CountryDao
}