package com.example.sixteenthtask

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CountryDao {
    @Insert
    suspend fun insertCountry(vararg country: Country)

    @Delete
    suspend fun delete(country: Country)

    @Query("SELECT * FROM Country")
    suspend fun getAll(): List<Country>
}